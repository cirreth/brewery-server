import brewery.remote.TestBrewery
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.After
import org.junit.Test
import repository.models.*
import system.AdjustableClock
import system.Context
import java.io.File
import java.time.Instant
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import kotlin.test.BeforeTest
import kotlin.test.assertNotEquals


class DatabaseTest {


    val testDbName = "brewery-test.db"
    var batch: Batch? = null
    var recipe = mapOf<String, RecipeStage>()
    val context = Context()
    val database = context.testDatabase.database


    @BeforeTest
    fun prepareDb() {
        transaction(database) {
            createRecipe()
        }
    }

    @After
    fun cleanUp() {
        File(testDbName).deleteOnExit()
    }

    @Test
    fun selectTest() {

//        transaction {
//
//            // check ingredient
//            val ingredient1Extracted = Ingredient[ingredient1]
//            assert(ingredient1Extracted.name == ename && ingredient1Extracted.unit == eunit)
//
//            // check recipe stages
//            val stages = RecipeStage.find { RecipeStages.recipe eq recipe1.id }
//            assert(stages.count().toInt() == 8)
//            assert(stages.first() == stageHeating0)
//            assert(stages.find { it.stageSeqNum == 2 } == stageRest1)
//
//        }

    }

    @Test
    fun clockTest() {
        val context = Context()
        val clock = context.adjustableClock as AdjustableClock
        assert(clock.instant().epochSecond == Instant.now().epochSecond)
        clock.plusMinutes(40)
        assertNotEquals(
            clock.instant().epochSecond == Instant.now().epochSecond + 30 * 60,
            clock.instant().epochSecond == Instant.now().epochSecond + 40 * 60
        )
    }



    @Test(timeout = 10000)
    fun testBreweryController() {

        val clock = context.adjustableClock as AdjustableClock
        val bc = context.testBreweryController
        val brewery = context.testBrewery as TestBrewery

            // pour brewery
            brewery.temperature = 26F
            // load recipe
            bc.brew(batch!!.id.value)
            // wait heating
            assert(bc.state.stage!!.id == recipe["stageHeating0"]!!.id)
            while (brewery.heaterPower != 1F) { Thread.sleep(0) }
            // wait accepting from user
            brewery.temperature = recipe["stageHeating0"]!!.temperature!!
            while (brewery.heaterPower != 0F) { Thread.sleep(0) }
            // too cold
            brewery.temperature = recipe["stageHeating0"]!!.temperature!! - 1
            while (brewery.heaterPower != 1F) { Thread.sleep(0) }
            // wait accepting by user
            brewery.temperature = recipe["stageHeating0"]!!.temperature!!
            while (brewery.heaterPower != 0F) { Thread.sleep(0) }
            brewery.temperature = recipe["stageHeating1"]!!.temperature!! - 3
            // rest loaded, temp too low
            while (bc.state.stage!!.id != recipe["stageAwaitrestLoaded"]!!.id) { Thread.sleep(0) }
            // accepted by brewer
            bc.accept()
            // check program next stage
            while (bc.state.stage!!.id != recipe["stageHeating1"]!!.id) { Thread.sleep(0) }
            while (brewery.heaterPower != 1F) { Thread.sleep(0) }
            // heating to rest temperature
            brewery.temperature = recipe["stageHeating1"]!!.temperature!!
            while (brewery.heaterPower != 0F) { Thread.sleep(0) }
            // heated, check program next stage
            while (bc.state.stage!!.id != recipe["stageRest1"]!!.id) { Thread.sleep(0) }
            // check rest end time + database select test
            transaction(database) {
                BatchLogItem.find { BatchLog.batch.eq(batch!!.id) and BatchLog.recipeStage.eq(bc.state.stage!!.id) }
            }
            assert(bc.state.stageEndTime!!.truncatedTo(ChronoUnit.MINUTES) == LocalDateTime.now(clock).plusMinutes(bc.state.stage!!.duration!!).truncatedTo(ChronoUnit.MINUTES))
            // time machine
            clock.plusMinutes(40)
            while (bc.state.stage!!.id != recipe["stageHeating2"]!!.id) { Thread.sleep(0) }
            // check heating to 2nd rest
            while (brewery.heaterPower != 1F) { Thread.sleep(0) }
            brewery.temperature = recipe["stageHeating2"]!!.temperature!!
            while (brewery.heaterPower != 0F) { Thread.sleep(0) }
            // check program next stage
            while (bc.state.stage!!.id != recipe["stageRest2"]!!.id) { Thread.sleep(0) }
            assert(bc.state.stageEndTime!!.truncatedTo(ChronoUnit.MINUTES) == LocalDateTime.now(clock).plusMinutes(bc.state.stage!!.duration!!).truncatedTo(ChronoUnit.MINUTES))
            // time machine
            clock.plusMinutes(20)
            //
            while (bc.state.stage!!.id != recipe["stageIodineTest"]!!.id) { Thread.sleep(0) }
            // accepted by brewer
            bc.accept()
            // mashout
            while (bc.state.stage!!.id != recipe["stageHeating3"]!!.id) { Thread.sleep(0) }
            brewery.temperature = recipe["stageHeating3"]!!.temperature!!
            while (bc.state.stage!!.id != recipe["stageRest3"]!!.id) { Thread.sleep(0) }
            clock.plusMinutes(1)
            // 
            while (bc.state.stage!!.id != recipe["stagePrepToBoiling"]!!.id) { Thread.sleep(0) }
            // accepted by brewer
            bc.accept()
            //
            while (bc.state.stage!!.id != recipe["stageBoiling"]!!.id) { Thread.sleep(0) }
            clock.plusMinutes(59)
            while (bc.state.stage!!.id != recipe["stageBoiling"]!!.id) { Thread.sleep(0) }
            clock.plusMinutes(1)
            while (bc.state.stage!!.id != recipe["stageCooling"]!!.id) { Thread.sleep(0) }
            brewery.temperature = recipe["stageCooling"]!!.temperature!!
            while (bc.state.stage != null) { Thread.sleep(0) }
    }

    private fun createRecipe() {

        val ename = "Viking Pale Ale"
        val eunit = "g"

        val ingredient1 = Ingredient.new {
            name = ename
            unit = eunit
        }

        // fake recipe
        val recipe0 = Recipe.new {
            name = "Fake recipe"
        }

        val fakeRecipeStage = RecipeStage.new {
            recipe = recipe0
            stageSeqNum = 0
            stageType = "heating"
            temperature = 68F
        }
        //

        val recipe1 = Recipe.new {
            name = "Hootch IPA"
        }

        val stageHeating0 = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 0
            stageType = "heating"
            temperature = 68F
        }

        val stageAwaitrestLoaded = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 1
            stageType = "await"
            temperature = 68F
        }

        val stageHeating1 = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 2
            stageType = "heating"
            temperature = 66F
        }

        val stageRest1 = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 3
            stageType = "rest"
            temperature = 66F
            duration = 40
        }

        val stageHeating2 = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 4
            stageType = "heating"
            temperature = 72F
        }

        val stageRest2 = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 5
            stageType = "rest"
            temperature = 72F
            duration = 19
        }

        val stageIodineTest = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 6
            stageType = "await"
        }

        val stageHeating3 = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 7
            stageType = "heating"
            temperature = 78F
        }

        val stageRest3 = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 8
            stageType = "rest"
            temperature = 78F
            duration = 1
        }

        val stagePrepToBoiling = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 9
            stageType = "await"
        }

        val stageBoiling = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 10
            stageType = "boiling"
            duration = 60
        }

        val stageCooling = RecipeStage.new {
            recipe = recipe1
            stageSeqNum = 11
            stageType = "cooling"
            temperature = 22F
        }

        val batch1 = Batch.new {
            recipe = recipe1
            date = LocalDateTime.now()
        }

        batch = batch1
        recipe = mapOf(
            "stageHeating0" to stageHeating0,
            "stageAwaitrestLoaded" to stageAwaitrestLoaded,
            "stageHeating1" to stageHeating1,
            "stageRest1" to stageRest1,
            "stageHeating2" to stageHeating2,
            "stageRest2" to stageRest2,
            "stageIodineTest" to stageIodineTest,
            "stageHeating3" to stageHeating3,
            "stageRest3" to stageRest3,
            "stagePrepToBoiling" to stagePrepToBoiling,
            "stageBoiling" to stageBoiling,
            "stageCooling" to stageCooling
            )

    }

}
