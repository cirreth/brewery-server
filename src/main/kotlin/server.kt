import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import models.api.BatchViewModel
import models.api.BreweryControllerStateViewModel
import models.api.RecipeStageViewModel
import models.api.RecipeViewModel
import org.slf4j.event.Level
import system.Context
import system.Log
import system.Messages


fun main() {


    val context = Context.instance
    embeddedServer(Netty, port = 8089) {
        install(ContentNegotiation) {
            gson()
        }
        install(CallLogging) {
            level = Level.INFO
        }
        routing {
            get("/recipes") {
                call.respond(context.breweryRepository.recipes())
            }
            post("/recipe") {
                val recipe = call.receive<RecipeViewModel>()
                Log.d("Recipe: ${recipe.name}")
                val recipeId = context.breweryRepository.writeRecipe(recipe)
                val vm = context.breweryRepository.readRecipe(recipeId)
                if (vm != null) {
                    call.respond(vm)
                } else {
                    call.respond(HttpStatusCode.NotFound, "Recipe not found")
                }
            }
            post("/brew") {
                val batchViewModel = call.receive<BatchViewModel>()
                if (batchViewModel.id == null) {
                    if (batchViewModel.recipe?.id != null) {
                        val batch = context.breweryRepository.createBatch(recipeId = batchViewModel.recipe.id)
                        context.breweryController.brew(batchId = batch.id!!)
                        call.respond(batchViewModel)
                    } else {
                        call.respond(HttpStatusCode.NotFound, Messages.RECIPE_NOT_FOUND)
                    }
                } else {
                    val batch = context.breweryRepository.getBatch(batchViewModel.id)
                    if (batch != null) {
                        context.breweryController.brew(batchViewModel.id)
                        call.respond(batch)
                    } else {
                        call.respond(HttpStatusCode.NotFound, Messages.BATCH_NOT_FOUND)
                    }
                }
            }
            post("/accept") {
                context.breweryController.accept()
                call.respond("OK")
            }
            get("/state") {
                val state = context.breweryController.state
                call.respond(BreweryControllerStateViewModel(
                    batch = BatchViewModel.fromBatch(state.batch, state.recipe),
                    stage = RecipeStageViewModel.fromRecipeStage(state.stage),
                    allowContinue = state.allowContinue
                ))
            }
            get("/cancel") {
                context.breweryController.cancel()
                call.respond(HttpStatusCode.OK)
            }
        }
    }.start(wait = true)
}
