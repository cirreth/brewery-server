package models.config

import kotlinx.serialization.Serializable

@Serializable
data class TelegramNotificatorConfig(
    val token: String
)
