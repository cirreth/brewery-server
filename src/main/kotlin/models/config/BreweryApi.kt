package models.config

import kotlinx.serialization.Serializable

@Serializable
data class BreweryApi(
    val getTemperature: String,
    val setHeaterOffState: String,
    val setHeaterOnState: String
)
