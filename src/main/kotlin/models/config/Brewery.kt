package models.config

import kotlinx.serialization.Serializable

@Serializable
data class Brewery(
    val address: String,
    val api: BreweryApi,
    val deltaBottom: Float,
    val deltaOverheat: Float,
    val pollingInterval: Long
)
