package models

import kotlinx.coroutines.Job
import repository.models.Batch
import repository.models.Recipe
import repository.models.RecipeStage
import java.time.LocalDateTime


data class BreweryControllerState(
    var recipe: Recipe? = null,
    var batch: Batch? = null,
    var stages: Iterable<RecipeStage> = emptyList(),
    var stage: RecipeStage? = null,
    var stageEndTime: LocalDateTime? = null,
    var cancelled: Boolean = true,
    var job: Job? = null,
    var allowContinue: Boolean = false

)
