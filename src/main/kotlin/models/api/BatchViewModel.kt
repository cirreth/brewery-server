package models.api

import repository.models.Batch
import repository.models.Recipe

data class BatchViewModel(
    val id: Int?,
    val recipe: RecipeViewModel?
) {
    companion object {
        fun fromBatch(batch: Batch?, recipe: Recipe?): BatchViewModel? {
            if (batch == null) return null
            return BatchViewModel(
                id = batch.id.value,
                recipe = RecipeViewModel.fromRecipe(recipe, emptyList())
            )
        }
    }

}

