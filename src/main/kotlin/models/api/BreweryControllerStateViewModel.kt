package models.api

data class BreweryControllerStateViewModel(
    val batch: BatchViewModel?,
    val stage: RecipeStageViewModel?,
    val allowContinue: Boolean
)
