package models.api

import kotlinx.serialization.Serializable
import repository.models.Recipe
import repository.models.RecipeStage
import repository.models.RecipeStages


@Serializable
data class RecipeViewModel(
    val id: Int?,
    val name: String,
    val stages: List<RecipeStageViewModel>
) {
    companion object {
        fun fromRecipe(recipe: Recipe?, stages: List<RecipeStage>): RecipeViewModel? {
            if (recipe == null) return null
            return RecipeViewModel(
                id = recipe.id.value,
                name = recipe.name,
                stages = stages.map {
                    RecipeStageViewModel(it.stageType, it.temperature, it.duration)
                })
        }
    }
}

@Serializable
data class RecipeStageViewModel(
    var stageType: String,
    var temperature: Float?,
    var duration: Long?
) {
    companion object {
        fun fromRecipeStage(recipeStage: RecipeStage?): RecipeStageViewModel? {
            if (recipeStage == null) return null
            return RecipeStageViewModel(
                stageType = recipeStage.stageType,
                temperature = recipeStage.temperature,
                duration = recipeStage.duration
            )
        }
    }
}
