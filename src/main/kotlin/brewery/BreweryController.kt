package brewery

import brewery.remote.Brewery
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import models.BreweryControllerState
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import repository.SqliteDatabase
import repository.models.*
import system.ConfigReader
import system.Log
import system.Messages
import system.Messages.Companion.NEW_BATCH_STARTED
import system.Messages.Companion.RESTORED_BATCH_CONTINUED
import system.Notificator
import java.time.Clock
import java.time.LocalDateTime

class BreweryController(override val kodein: Kodein) : KodeinAware {

    private val configReader: ConfigReader by kodein.instance()
    private val breweryConfig = configReader.read().brewery
    private val brewery: Brewery by kodein.instance()
    private val notificator: Notificator by kodein.instance()
    private val clock: Clock by kodein.instance()
    private val db: SqliteDatabase by kodein.instance()

    var state = BreweryControllerState()

    fun brew(batchId: Int) {
        transaction(db.database) {
            if (restoreState(batchId) == null) {
                state.batch = Batch[batchId]
                state.recipe = state.batch!!.recipe
                state.stages = RecipeStage.find { RecipeStages.recipe eq state.recipe!!.id }
                nextStage()
                notificator.notificate(NEW_BATCH_STARTED + " ${state.batch!!.recipe.name}")
            } else {
                notificator.notificate(RESTORED_BATCH_CONTINUED + " ${state.batch!!.recipe.name}")
            }
        }
        state.job = start()
    }

    private fun start(): Job {
        state.cancelled = false
        return GlobalScope.launch {
            while (!state.cancelled) {
                step()
                delay(breweryConfig.pollingInterval)
            }
            state = BreweryControllerState()
        }
    }

    private suspend fun step() {
        try {
            when (state.stage?.stageType) {
                "heating" -> {
                    val temp = brewery.getTemperatureAsync().await()
                    if (temp >= state.stage!!.temperature!!) {
                        val heaterState = brewery.setHeaterOffAsync().await()
                        if (heaterState != 0) {
                            notificator.notificate(Messages.HEATER_STATE_IS_INCORRECT)
                        }
                        nextStage()
                    } else {
                        val heaterState = brewery.setHeaterOnAsync().await()
                        if (heaterState != 1) {
                            notificator.notificate(Messages.HEATER_STATE_IS_INCORRECT)
                        }
                    }
                }
                "rest" -> {
                    if (LocalDateTime.now(clock) >= state.stageEndTime) {
                        nextStage()
                    } else {
                        val temp = brewery.getTemperatureAsync().await()
                        if (temp >= state.stage!!.temperature!!) {
                            val heaterState = brewery.setHeaterOffAsync().await()
                            if (heaterState != 0) {
                                notificator.notificate(Messages.HEATER_STATE_IS_INCORRECT)
                            }
                        } else {
                            val heaterState = brewery.setHeaterOnAsync().await()
                            if (heaterState != 1) {
                                notificator.notificate(Messages.HEATER_STATE_IS_INCORRECT)
                            }
                        }
                    }
                }
                "await" -> {
                    if (state.allowContinue) nextStage()
                    val temp = brewery.getTemperatureAsync().await()
                    if (state.stage!!.temperature != null && temp < state.stage!!.temperature!!) {
                        val heaterState = brewery.setHeaterOnAsync().await()
                        if (heaterState != 1) {
                            notificator.notificate(Messages.HEATER_STATE_IS_INCORRECT)
                        }
                    } else {
                        val heaterState = brewery.setHeaterOffAsync().await()
                        if (heaterState != 0) {
                            notificator.notificate(Messages.HEATER_STATE_IS_INCORRECT)
                        }
                    }
                }
                "boiling" -> {
                    if (LocalDateTime.now(clock) >= state.stageEndTime) {
                        nextStage()
                    }
                }
                "cooling" -> {
                    val temp = brewery.getTemperatureAsync().await()
                    if (temp <= state.stage!!.temperature!!) {
                        nextStage()
                    }
                }
            }
        } catch (e: Exception) {
            notificator.notificate("Error in main loop: ${e.message}")
        }

    }

    private fun nextStage() {
        state.allowContinue = false
        val prevStage = state.stage
        if (state.stage == null) {
            state.stage = state.stages.sortedBy { it.stageSeqNum }.first()
            writeBatchLog(prevStage, state.stage)
            return
        }
        if (state.stage?.stageType == "cooling") {
            notificator.notificate(Messages.COOLING_COMPLETED)
        }
        transaction(db.database) {
            val newStage = state.stages.firstOrNull { it.stageSeqNum == state.stage!!.stageSeqNum + 1}
            state.stageEndTime = if (newStage?.duration != null) LocalDateTime.now(clock).plusMinutes(newStage.duration!!) else null
            writeBatchLog(prevStage, newStage)
            //notificator.notificate("From stage ${prevStage?.stageSeqNum} (${prevStage?.stageType}) to stage ${newStage?.stageSeqNum} (${newStage?.stageType}) end time $state.stageEndTime")
            state.stage = newStage
            if (newStage != null) {
                notificator.notificate(Messages.NEW_STAGE + " ${newStage.stageType}")
            } else {
                notificator.notificate(Messages.PROCESS_ENDED + " ${newStage?.stageType}")
            }
            if (newStage?.stageType == "await") {
                notificator.notificate(Messages.ACCEPT_REQUIRED)
            }
        }
    }

    private fun writeBatchLog(prevStage: RecipeStage?, newStage: RecipeStage?) {
        if (prevStage != null) {
            transaction(db.database) {
                BatchLog.update({ BatchLog.batch.eq(state.batch!!.id) and BatchLog.recipeStage.eq(state.stage!!.id) }) {
                    it[endDate] = LocalDateTime.now(clock)
                }
            }
        }
        if (newStage == null) return
        val currentBatch = state.batch!!
        transaction(db.database) {
            BatchLogItem.new {
                batch = currentBatch
                recipeStage = newStage
                startDate = LocalDateTime.now(clock)
            }
        }
    }

    fun pause() {}

    fun resume() {}

    fun cancel() {
        state.cancelled = true
    }

    fun accept() {
        state.allowContinue = true
    }

    private fun restoreState(batchId: Int): Batch? {
        return transaction(db.database) {
            val lastBatchLogItem = BatchLogItem
                .find { BatchLog.batch eq batchId }
                .sortedByDescending { BatchLog.startDate }
                .firstOrNull()
            if (lastBatchLogItem != null) {
                Log.i("Restoring previous batch $batchId")
                val batch = lastBatchLogItem.batch
                val recipe = batch.recipe
                state = BreweryControllerState(
                    recipe = recipe,
                    batch = batch,
                    stages = RecipeStage.find { RecipeStages.recipe eq recipe.id }.sortedBy { RecipeStages.stageSeqNum },
                    stage = lastBatchLogItem.recipeStage,
                    stageEndTime =  lastBatchLogItem.endDate,
                    cancelled = false,
                    job = null,
                    allowContinue = false
                )
                return@transaction lastBatchLogItem.batch
            }
            return@transaction null
        }
    }

}