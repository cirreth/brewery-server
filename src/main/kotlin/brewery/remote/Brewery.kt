package brewery.remote

import kotlinx.coroutines.Deferred

interface Brewery {

    fun getTemperatureAsync(): Deferred<Float>

    fun setHeaterOnAsync(): Deferred<Int>

    fun setHeaterOffAsync(): Deferred<Int>

    fun getHeaterPowerAsync(): Deferred<Float>

    fun setHeaterPowerAsync(power: Float): Deferred<Float>

    fun setPumpOnAsync(): Deferred<Int>

    fun setPumpOffAsync(): Deferred<Int>

}