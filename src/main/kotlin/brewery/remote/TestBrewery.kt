package brewery.remote

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job

class TestBrewery: Brewery {

    var temperature: Float = 25F
    var pumpState: Int = 0
    var heaterPower: Float = 0F

    override fun getTemperatureAsync(): Deferred<Float> {
        return CompletableDeferred(temperature)
    }

    override fun setHeaterOnAsync(): Deferred<Int> {
        heaterPower = 1F
        return CompletableDeferred(heaterPower.toInt())
    }

    override fun setHeaterOffAsync(): Deferred<Int> {
        heaterPower = 0F
        return CompletableDeferred(heaterPower.toInt())
    }

    override fun getHeaterPowerAsync(): Deferred<Float> {
        return CompletableDeferred(heaterPower)
    }

    override fun setHeaterPowerAsync(power: Float): Deferred<Float> {
        heaterPower = power
        return CompletableDeferred(heaterPower)
    }

    override fun setPumpOnAsync(): Deferred<Int> {
        pumpState = 1
        return CompletableDeferred(pumpState)
    }

    override fun setPumpOffAsync(): Deferred<Int> {
        pumpState = 0
        return CompletableDeferred(pumpState)
    }


}