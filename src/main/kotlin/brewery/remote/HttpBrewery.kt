package brewery.remote

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import kotlinx.coroutines.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import system.ConfigReader


class HttpBrewery(override val kodein: Kodein) : Brewery, KodeinAware {

    private val configReader: ConfigReader by kodein.instance()
    private val brewery = configReader.read().brewery
    private val breweryApi = configReader.read().brewery.api
    private val httpClient = HttpClient(CIO) {
        expectSuccess = false
        engine {
            endpoint {
                keepAliveTime = 0
            }
        }
    }

    override fun getTemperatureAsync(): Deferred<Float> = CoroutineScope(Dispatchers.IO).async {
        httpClient.get("${brewery.address}${breweryApi.getTemperature}")
    }

    override fun setHeaterOnAsync(): Deferred<Int> = CoroutineScope(Dispatchers.IO).async {
        httpClient.get("${brewery.address}${breweryApi.setHeaterOnState}")
    }

    override fun setHeaterOffAsync(): Deferred<Int> = CoroutineScope(Dispatchers.IO).async {
        httpClient.get("${brewery.address}${breweryApi.setHeaterOffState}")
    }

    override fun getHeaterPowerAsync(): Deferred<Float> {
        TODO("Not yet implemented")
    }

    override fun setHeaterPowerAsync(power: Float): Deferred<Float> {
        TODO("Not yet implemented")
    }

    override fun setPumpOnAsync(): Deferred<Int> {
        TODO("Not yet implemented")
    }

    override fun setPumpOffAsync(): Deferred<Int> {
        TODO("Not yet implemented")
    }

}