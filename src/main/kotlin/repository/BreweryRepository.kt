package repository

import models.api.BatchViewModel
import models.api.RecipeViewModel
import org.jetbrains.exposed.sql.transactions.transaction
import repository.models.Batch
import repository.models.Recipe
import repository.models.RecipeStage
import repository.models.RecipeStages
import java.time.LocalDateTime

class BreweryRepository(private val db: SqliteDatabase) {

    fun createBatch(recipeId: Int): BatchViewModel = transaction(db.database) {
        val batch = Batch.new {
            recipe = Recipe[recipeId]
            date = LocalDateTime.now()
        }
        return@transaction BatchViewModel(
            id = batch.id.value,
            recipe = readRecipe(recipeId))
    }

    fun getBatch(batchId: Int): BatchViewModel? = transaction(db.database) {
    val batch = Batch.findById(batchId) ?: return@transaction null
    return@transaction BatchViewModel(
        id = batch.id.value,
        recipe = readRecipe(batch.recipe.id.value)
        )
    }

    fun recipes(): List<RecipeViewModel> = transaction(db.database) {
        return@transaction Recipe.all().map {
            readRecipe(it.id.value)!!
        }
    }

    fun writeRecipe(rvm: RecipeViewModel): Int {
        return transaction(db.database) {
            val newRecipe = Recipe.new {
                name = rvm.name
            }
            rvm.stages.mapIndexed { idx, stageVM ->
                RecipeStage.new {
                    recipe = newRecipe
                    stageSeqNum = idx
                    stageType = stageVM.stageType
                    temperature = stageVM.temperature
                    duration = stageVM.duration
                }
            }
            return@transaction newRecipe.id.value
        }
    }

    fun readRecipe(recipeId: Int): RecipeViewModel? {
        return transaction(db.database) {
            val recipe = Recipe.findById(recipeId) ?: return@transaction null
            val recipeStages = RecipeStage
                .find { RecipeStages.recipe eq recipe.id }
                .sortedBy { RecipeStages.stageSeqNum }
            return@transaction RecipeViewModel.fromRecipe(recipe, recipeStages)
        }
    }

}