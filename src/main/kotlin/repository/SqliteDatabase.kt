package repository

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import repository.models.*

class SqliteDatabase(filename: String) {

    val database: Database = Database.connect("jdbc:sqlite:$filename", driver = "org.sqlite.JDBC")

    init {
        transaction {
            addLogger(StdOutSqlLogger)
            SchemaUtils.create(Ingredients, Recipes, RecipeStages, Batches, BatchLog, NotificationClients)
        }
    }

}
