package repository.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object NotificationClients: IntIdTable() {
    val token = varchar("token", 200).uniqueIndex()
}

class NotificationClient(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<NotificationClient>(NotificationClients)
    var token by NotificationClients.token
}
