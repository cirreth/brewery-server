package repository.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.`java-time`.datetime

object BatchLog: IntIdTable() {
    val batch = reference("batch", Batches)
    val recipeStage = reference("recipeStage", RecipeStages)
    val startDate = datetime("startDate")
    val endDate = datetime("endDate").nullable()
}

class BatchLogItem(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<BatchLogItem>(BatchLog)
    var batch by Batch referencedOn BatchLog.batch
    var recipeStage by RecipeStage referencedOn BatchLog.recipeStage
    var startDate by BatchLog.startDate
    var endDate by BatchLog.endDate
}
