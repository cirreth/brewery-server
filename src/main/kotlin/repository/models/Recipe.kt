package repository.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.`java-time`.datetime
import java.time.LocalDateTime

object Recipes: IntIdTable() {
    var name = varchar("name", 200)
    var createDate = datetime("date").default(LocalDateTime.now())
}

class Recipe(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Recipe>(Recipes)
    var name by Recipes.name
}
