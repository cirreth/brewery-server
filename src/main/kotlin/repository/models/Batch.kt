package repository.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.`java-time`.datetime

object Batches: IntIdTable() {
    val recipe = reference("recipe", Recipes)
    val date = datetime("batchDate")
}

class Batch(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Batch>(Batches)
    var recipe by Recipe referencedOn Batches.recipe
    var date by Batches.date
}
