package repository.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Ingredients: IntIdTable() {
    val name = varchar("name", 100)
    val unit = varchar("unit", 20)
}

class Ingredient(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Ingredient>(Ingredients)
    var name by Ingredients.name
    var unit by Ingredients.unit
}
