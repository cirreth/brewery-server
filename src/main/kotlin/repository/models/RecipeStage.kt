package repository.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object RecipeStages: IntIdTable() {
    var recipe = reference("recipe", Recipes)
    var stageSeqNum = integer("stageSeqNum")
    var stageType = varchar("stageType", 20)
    var temperature = float("temperature").nullable()
    var duration = long("duration").nullable()
    // val ingredients = ...
}

class RecipeStage(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<RecipeStage>(RecipeStages)
    var recipe by Recipe referencedOn RecipeStages.recipe
    var stageSeqNum by RecipeStages.stageSeqNum
    var stageType by RecipeStages.stageType
    var temperature by RecipeStages.temperature
    var duration by RecipeStages.duration
}
