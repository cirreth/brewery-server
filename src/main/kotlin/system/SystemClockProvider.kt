package system

import java.time.Clock
import java.time.Instant
import java.time.ZoneId
import java.time.temporal.TemporalUnit

class AdjustableClock: Clock() {

    private var instant = Instant.now()

    override fun getZone(): ZoneId {
        return ZoneId.systemDefault()
    }

    override fun withZone(zone: ZoneId?): Clock {
        return fixed(instant, zone ?: ZoneId.systemDefault())
    }

    override fun instant(): Instant {
        return instant
    }

    fun plusMinutes(minutes: Int) {
        instant = instant.plusSeconds(60*minutes.toLong())
    }

}