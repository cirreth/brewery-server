package system

import org.slf4j.LoggerFactory

class Log {

    companion object {

        private val logger = LoggerFactory.getLogger(Log::class.java)

        fun d(msg: String) { logger.debug(msg) }
        fun i(msg: String) { logger.info(msg) }
        fun w(msg: String) { logger.warn(msg) }
        fun e(msg: String) { logger.error(msg) }

    }

}