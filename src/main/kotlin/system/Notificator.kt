package system

import brewery.BreweryController
import dev.inmo.tgbotapi.bot.Ktor.telegramBot
import dev.inmo.tgbotapi.extensions.api.chat.get.getChat
import dev.inmo.tgbotapi.extensions.api.send.reply
import dev.inmo.tgbotapi.extensions.api.send.sendMessage
import dev.inmo.tgbotapi.extensions.behaviour_builder.buildBehaviour
import dev.inmo.tgbotapi.extensions.behaviour_builder.triggers_handling.onCommand
import dev.inmo.tgbotapi.types.ChatId
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.exposed.sql.transactions.transaction
import repository.SqliteDatabase
import repository.models.NotificationClient
import java.time.LocalDateTime
import java.util.*

interface Notificator {

    fun notificate(message: String)

}

class ConsoleNotificator: Notificator {

    override fun notificate(message: String) {
        Log.e(message.take(200))
    }

}

class TelegramNotificator(val configReader: ConfigReader,
                          val db: SqliteDatabase,
                          val breweryController: BreweryController
                          ): Notificator {

    val tgConfig= configReader.read().telegramNotificator
    val bot = telegramBot(tgConfig.token)
    val queue = mutableListOf<String>()
    var knownChats = mutableListOf<Long>()
    val cancelled: Boolean = false
    var blockedBefore: LocalDateTime? = null

    init {
        initialize()
    }

    override fun notificate(message: String) {
        queue.add(message)
    }

    private fun initialize(){
        transaction(db.database) {
            knownChats = NotificationClient.all().map { it.token.toLong() }.toMutableList()
        }
        GlobalScope.launch {
            while (!cancelled) {
                delay(1000)
                if (blockedBefore != null && LocalDateTime.now() < blockedBefore) continue
                blockedBefore = LocalDateTime.now().plusSeconds(15)
                if (queue.size > 0) {
                    val msg = queue.removeFirst()
                    knownChats.forEach {
                        bot.sendMessage(bot.getChat(ChatId(it)), msg)
                        if (msg.contains(Messages.ACCEPT_REQUIRED)) {
                            bot.sendMessage(bot.getChat(ChatId(it)), "/accept")
                        }
                    }
                    queue.clear()
                }
            }
        }.start()
        GlobalScope.launch {
            bot.buildBehaviour(this) {
                this.onCommand("start") {
                    reply(it, "connected")
                    Log.i("Notificator: new telegram subscriber found: ${it.chat.id}")
                    transaction(db.database) {
                        try {
                            NotificationClient.new {
                                token = it.chat.id.chatId.toString()
                            }
                            knownChats.add(it.chat.id.chatId)
                        } catch (err: Exception) {
                            Log.e("Error on save Telegram token: ${err.message}")
                        }
                    }
                }
                this.onCommand("accept") {
                    reply(it, "ok")
                    breweryController.accept()
                }
            }.join()
        }
    }

}