package system

import com.charleskorn.kaml.Yaml
import java.io.File


interface ConfigReader {

    fun read(): Config

}

class FileConfigReader(var filename: String): ConfigReader {

    var cachedConfig: Config? = null

    override fun read(): Config {
        if (cachedConfig != null) return cachedConfig!!
        return Yaml.default.decodeFromString(Config.serializer(), File(filename).readText())
    }

}
