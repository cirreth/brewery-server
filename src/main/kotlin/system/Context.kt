package system
import brewery.BreweryController
import brewery.remote.Brewery
import brewery.remote.HttpBrewery
import brewery.remote.TestBrewery
import org.kodein.di.Kodein
import org.kodein.di.KodeinTrigger
import org.kodein.di.generic.bind
import org.kodein.di.generic.eagerSingleton
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import repository.BreweryRepository
import repository.SqliteDatabase
import java.time.Clock

class Context {

    companion object {
        val instance = Context()
    }

    private val kodein = Kodein {
        bind<ConfigReader>() with singleton { FileConfigReader("exposed/config.yaml") }
        bind<Brewery>() with singleton { TestBrewery() }
        bind<SqliteDatabase>() with singleton { SqliteDatabase("brewery.db") }
        bind<Notificator>() with eagerSingleton { TelegramNotificator(instance(), instance(), instance()) }
        bind<Clock>() with singleton { Clock.systemDefaultZone() }
        bind<BreweryController>() with singleton { BreweryController(kodein) }
        bind<BreweryRepository>() with singleton { BreweryRepository(instance()) }
    }

    val initTrigger = KodeinTrigger()

    val configReader:  ConfigReader by kodein.instance()
    val breweryController: BreweryController by kodein.instance()
    val breweryRepository: BreweryRepository by kodein.instance()

    // tests

    private val testKodein = Kodein {
        extend(kodein)
        bind<ConfigReader>(overrides = true) with singleton { FileConfigReader("exposed/config.test.yaml") }
        bind<Clock>(overrides = true) with singleton { AdjustableClock() }
        bind<Brewery>(overrides = true) with singleton { TestBrewery() }
        bind<Notificator>(overrides = true) with singleton { ConsoleNotificator() }
        bind<BreweryController>(overrides = true) with singleton { BreweryController(this.kodein) }
        bind<SqliteDatabase>(overrides = true) with singleton { SqliteDatabase("brewery.test.db") }
    }

    val testBreweryController: BreweryController by testKodein.instance()
    val adjustableClock: Clock by testKodein.instance()
    val testBrewery: Brewery by testKodein.instance()
    val testDatabase: SqliteDatabase by testKodein.instance()

}
