package system

import models.config.Brewery
import kotlinx.serialization.Serializable
import models.config.TelegramNotificatorConfig


@Serializable
data class Config(
    var brewery: Brewery,
    var telegramNotificator: TelegramNotificatorConfig
)
