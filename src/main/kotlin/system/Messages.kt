package system

class Messages {
    companion object {
        const val HEATER_STATE_IS_INCORRECT = "Состояние нагревателя не соответствует запрошенному"
        const val PROCESS_ENDED = "Варка завершена"
        const val NEW_BATCH_STARTED = "Начата варка новой партии. Рецепт: "
        const val RESTORED_BATCH_CONTINUED = "Продолжена варка предыдущей партии. Рецепт: "
        const val RECIPE_NOT_FOUND = "Рецепт не найден"
        const val BATCH_NOT_FOUND = "Партия не найдена"
        const val ACCEPT_REQUIRED = "Подтвердите завершение этапа, чтобы продолжить"
        const val COOLING_COMPLETED = "Охлаждение завершено"
        const val NEW_STAGE = "Переход на этап "
    }

}