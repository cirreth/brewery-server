ARG VERSION=8u151

FROM openjdk:${VERSION}-jdk as BUILD

COPY . /src
WORKDIR /src
RUN bash gradlew build
RUN mkdir /dist && tar -xf build/distributions/*.tar -C /dist

FROM openjdk:${VERSION}-jre

COPY --from=BUILD /dist/* /app
WORKDIR /app

CMD ["/app/bin/brewery"]
